'use strict';
jQuery(document).ready(function ($) {
    let $button_download = $('.vi-wad-action-download');
    let $button_download_all = $('.vi-wad-action-download-all');
    let $button_delete = $('.vi-wad-action-delete');
    let $button_delete_all = $('.vi-wad-action-delete-all');
    $button_download_all.on('click', function () {
        $('.vi-wad-action-download').click();
    });
    $button_delete_all.on('click', function () {
        if (confirm(vi_wad_params_admin_error_images.i18n_confirm_delete_all)) {
            $('.vi-wad-action-delete').map(function () {
                let $button = $(this);
                let $row = $button.closest('tr');
                let item_id = $button.data('item_id');
                if (!$button.hasClass('loading')) {
                    $button.addClass('loading');
                    $.ajax({
                        url: vi_wad_params_admin_error_images.url,
                        type: 'POST',
                        dataType: 'JSON',
                        data: {
                            action: 'vi_wad_delete_error_product_images',
                            item_id: item_id
                        },
                        success: function (response) {
                            $button.removeClass('loading');
                            if (response.status === 'success') {
                                $row.remove();
                            }
                        },
                        error: function (err) {
                            console.log(err);
                            $button.removeClass('loading');
                        }
                    })
                }
            })
        }
    });
    $button_delete.on('click', function () {
        let $button = $(this);
        let $row = $button.closest('tr');
        let item_id = $button.data('item_id');
        if ($button.hasClass('loading')) {
            return;
        }
        if (confirm(vi_wad_params_admin_error_images.i18n_confirm_delete)) {
            $button.addClass('loading');
            $button.find('.vi-wad-delete-image-error').remove();
            $.ajax({
                url: vi_wad_params_admin_error_images.url,
                type: 'POST',
                dataType: 'JSON',
                data: {
                    action: 'vi_wad_delete_error_product_images',
                    item_id: item_id
                },
                success: function (response) {
                    $button.removeClass('loading');
                    if (response.status === 'success') {
                        $row.remove();
                    } else {
                        let $result_icon = $('<span class="vi-wad-delete-image-error dashicons dashicons-no" title="' + response.message + '"></span>');
                        $button.append($result_icon);
                    }
                },
                error: function (err) {
                    console.log(err);
                    $button.removeClass('loading');
                }
            })
        }
    });
    $button_download.on('click', function () {
        let $button = $(this);
        let $row = $button.closest('tr');
        let item_id = $button.data('item_id');
        if ($button.hasClass('loading')) {
            return;
        }
        $button.addClass('loading');
        $button.find('.vi-wad-download-image-error').remove();
        $.ajax({
            url: vi_wad_params_admin_error_images.url,
            type: 'POST',
            dataType: 'JSON',
            data: {
                action: 'vi_wad_download_error_product_images',
                item_id: item_id
            },
            success: function (response) {
                $button.removeClass('loading');
                if (response.status === 'success') {
                    $row.remove();
                } else {
                    let $result_icon = $('<span class="vi-wad-download-image-error dashicons dashicons-no" title="' + response.message + '"></span>');
                    $button.append($result_icon);
                }
            },
            error: function (err) {
                console.log(err);
                $button.removeClass('loading');
            }
        })
    })
});
