<?php
if ( ! class_exists( 'Vi_WAD_Background_Download_Description' ) ) {
	class Vi_WAD_Background_Download_Description extends WP_Background_Process {

		use Vi_WAD_Functions;

		/**
		 * @var string
		 */
		protected $action = 'vi_wad_download_description';

		/**
		 * Task
		 *
		 * Override this method to perform any actions required on each
		 * queue item. Return the modified item for further processing
		 * in the next pass through. Or, return false to remove the
		 * item from the queue.
		 *
		 * @param mixed $item Queue item to iterate over
		 *
		 * @return mixed
		 */
		protected function task( $item ) {
			try {
				$settings            = new VI_WOO_ALIDROPSHIP_DATA();
				$description_url     = isset( $item['description_url'] ) ? $item['description_url'] : '';
				$product_id          = isset( $item['product_id'] ) ? $item['product_id'] : '';
				$description         = isset( $item['description'] ) ? $item['description'] : '';
				$product_description = isset( $item['product_description'] ) ? $item['product_description'] : '';
				if ( $description_url && $product_id ) {
					$request = wp_remote_get(
						$description_url,
						array(
							'user-agent' => $settings::get_user_agent(),
							'timeout'    => 60,
						)
					);
					if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {
						if ( isset( $request['body'] ) && $request['body'] ) {
							$body = preg_replace( '/<script\>[\s\S]*?<\/script>/im', '', $request['body'] );
							preg_match_all( '/src="([\s\S]*?)"/im', $body, $matches );
							if ( isset( $matches[1] ) && is_array( $matches[1] ) && count( $matches[1] ) ) {
								update_post_meta( $product_id, '_vi_wad_description_images', $matches[1] );
							}
							$str_replace = $settings->get_params( 'string_replace' );
							if ( isset( $str_replace['to_string'] ) && is_array( $str_replace['to_string'] ) && $str_replace_count = count( $str_replace['to_string'] ) ) {
								for ( $i = 0; $i < $str_replace_count; $i ++ ) {
									if ( $str_replace['sensitive'][ $i ] ) {
										$body = str_replace( $str_replace['from_string'][ $i ], $str_replace['to_string'][ $i ], $body );
									} else {
										$body = str_ireplace( $str_replace['from_string'][ $i ], $str_replace['to_string'][ $i ], $body );
									}
								}

							}
							if ( $product_description == 'item_specifics_and_description' || $product_description == 'description' ) {
								$description .= $body;
								wp_update_post( array( 'ID' => $product_id, 'post_content' => $description ) );
							}
						}
					}
				}

				return false;
			} catch ( Exception $e ) {
				return false;
			}
		}

		/**
		 * Is the updater running?
		 *
		 * @return boolean
		 */
		public function is_downloading() {
			global $wpdb;

			$table  = $wpdb->options;
			$column = 'option_name';

			if ( is_multisite() ) {
				$table  = $wpdb->sitemeta;
				$column = 'meta_key';
			}

			$key = $wpdb->esc_like( $this->identifier . '_batch_' ) . '%';


			return boolval( $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM {$table} WHERE {$column} LIKE %s", $key ) ) );
		}

		/**
		 * Complete
		 *
		 * Override if applicable, but ensure that the below actions are
		 * performed, or, call parent::complete().
		 */
		protected function complete() {
			// Show notice to user or perform some other arbitrary task...
//			add_action('admin_init','');
			parent::complete();
		}

		/**
		 * Delete all batches.
		 *
		 * @return Vi_WAD_Background_Download_Description
		 */
		public function delete_all_batches() {
			global $wpdb;

			$table  = $wpdb->options;
			$column = 'option_name';

			if ( is_multisite() ) {
				$table  = $wpdb->sitemeta;
				$column = 'meta_key';
			}

			$key = $wpdb->esc_like( $this->identifier . '_batch_' ) . '%';

			$wpdb->query( $wpdb->prepare( "DELETE FROM {$table} WHERE {$column} LIKE %s", $key ) ); // @codingStandardsIgnoreLine.

			return $this;
		}

		/**
		 * Kill process.
		 *
		 * Stop processing queue items, clear cronjob and delete all batches.
		 */
		public function kill_process() {
			if ( ! $this->is_queue_empty() ) {
				$this->delete_all_batches();
				wp_clear_scheduled_hook( $this->cron_hook_identifier );
			}
		}
	}
}
