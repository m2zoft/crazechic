<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class VI_WOO_ALIDROPSHIP_DATA {
	private static $prefix;
	private $params;
	private $default;

	/**
	 * VI_WOO_ALIDROPSHIP_DATA constructor.
	 */
	public function __construct() {
		self::$prefix = 'vi-wad-';
		global $wooaliexpressdropship_settings;
		if ( ! $wooaliexpressdropship_settings ) {
			$wooaliexpressdropship_settings = get_option( 'wooaliexpressdropship_params', array() );
		}
		$this->default = array(
			'enable'                   => '1',
			'secret_key'               => '',
			'product_status'           => 'publish',
			'catalog_visibility'       => 'visible',
			'product_gallery'          => 1,
			'product_categories'       => array(),
			'product_tags'             => array(),
			'product_description'      => 'item_specifics',
			'variation_visible'        => '',
			'manage_stock'             => '1',
			'ignore_ship_from'         => 0,
			'price_from'               => array( 0 ),
			'plus_value'               => array( 200 ),
			'plus_sale_value'          => array( - 1 ),
			'plus_value_type'          => array( 'percent' ),
			'import_product_currency'  => 'USD',
			'import_currency_rate'     => '1',
			'fulfill_default_carrier'  => 'EMS_ZX_ZX_US',
			'fulfill_order_note'       => 'I\'m dropshipping. Please DO NOT put any invoices, QR codes, promotions or your brand name logo in the shipments. Please ship as soon as possible for repeat business. Thank you!',
			'order_status_for_fulfill' => array( 'wc-completed', 'wc-on-hold', 'wc-processing' ),
			'order_status_after_sync'  => 'wc-completed',
			'string_replace'           => array(),
			'carrier_name_replaces'    => array(
				'from_string' => array(),
				'to_string'   => array(),
				'sensitive'   => array(),
			),
			'carrier_url_replaces'     => array(
				'from_string' => array(),
				'to_string'   => array(),
			),
		);

		$this->params = apply_filters( 'wooaliexpressdropship_params', wp_parse_args( $wooaliexpressdropship_settings, $this->default ) );

//		add_filter( 'vi_wad_manage_stock_option', array( $this, 'manage_stock' ) );
	}

	public static function get_attribute_name_by_slug( $slug ) {
		return ucwords( str_replace( '-', ' ', $slug ) );
	}

	/**
	 * @param $url
	 *
	 * @return mixed
	 */
	public static function get_domain_from_url( $url ) {
		$url     = strtolower( $url );
		$url_arr = explode( '//', $url );
		if ( count( $url_arr ) > 1 ) {
			$url = str_replace( 'www.', '', $url_arr[1] );

		} else {
			$url = str_replace( 'www.', '', $url_arr[0] );
		}
		$url_arr = explode( '/', $url );
		$url     = $url_arr[0];

		return $url;
	}

	/**
	 * @param array $args
	 * @param bool $return_sku
	 *
	 * @return array
	 */
	public static function get_imported_products( $args = array(), $return_sku = false ) {
		$imported_products = array();
		$args              = wp_parse_args( $args, array(
			'post_type'      => 'vi_wad_draft_product',
			'posts_per_page' => - 1,
			'meta_key'       => '_vi_wad_sku',
			'post_status'    => array(
				'publish',
				'draft',
				'override'
			),
			'fields'         => 'id'
		) );

		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
			if ( $return_sku ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$product_id  = get_the_ID();
					$product_sku = get_post_meta( $product_id, '_vi_wad_sku', true );
					if ( $product_sku ) {
						$imported_products[] = $product_sku;
					}
				}
			} else {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$imported_products[] = get_the_ID();
				}
			}
		}
		wp_reset_postdata();

		return $imported_products;
	}

	public static function get_woo_id_by_ali_product_sku( $sku, $post_type = 'product' ) {
		$return = '';
		if ( $sku ) {
			$args = array(
				'post_type'      => $post_type,
				'post_status'    => array( 'publish', 'private', 'pending' ),
				'posts_per_page' => 1,
				'meta_value'     => $sku,
			);
			if ( $post_type == 'product' ) {
				$args['meta_key'] = '_vi_wad_aliexpress_product_id';
			} else {
				$args['meta_key'] = '_vi_wad_aliexpress_variation_id';
			}
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$return = get_post_meta( get_the_ID(), '_vi_wad_woo_id', true );
					break;
				}
			}
			wp_reset_postdata();
		}

		return $return;
	}

	public static function get_id_by_ali_product_sku( $sku, $post_status = array( 'publish', 'draft', 'override' ) ) {
		$return = '';
		if ( $sku ) {
			$args = array(
				'post_type'      => 'vi_wad_draft_product',
				'posts_per_page' => 1,
				'meta_key'       => '_vi_wad_sku',
				'meta_value'     => $sku,
				'post_status'    => $post_status,
			);

			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					$return = get_the_ID();
					break;
				}
			}
			wp_reset_postdata();
		}

		return $return;
	}

	public static function modify_cookie( $cookie ) {
		if ( $cookie ) {
			$cookie_ar   = explode( '&', $cookie );
			$new_cookies = array();
			foreach ( $cookie_ar as $cookie_ar_k => $cookie_ar_v ) {
				$cookie_pat = explode( '=', $cookie_ar_v );
				switch ( $cookie_pat[0] ) {
					case 'site';
						$cookie_pat[1] = 'glo_v';
						break;
					case 'c_tp';
						$cookie_pat[1] = 'USD';
						break;
					default:
				}

				$new_cookies[] = implode( '=', $cookie_pat );
			}

			return implode( '&', $new_cookies );
		} else {
			return $cookie;
		}
	}

	public static function get_data( $url, $args = array(), $html = '' ) {
		$response   = array(
			'status'  => 'success',
			'message' => '',
			'data'    => array(),
		);
		$attributes = array(
			'sku' => '',
		);
		if ( ! $html ) {
			$args    = wp_parse_args( $args, array(
				'user-agent' => self::get_user_agent(),
				'timeout'    => 1000,
			) );
			$request = wp_remote_get( $url, $args );

			if ( ! is_wp_error( $request ) || wp_remote_retrieve_response_code( $request ) === 200 ) {

				$html = $request['body'];

			} else {
				$response['status']  = 'error';
				$response['message'] = $request->get_error_messages();

				return $response;
			}
		}
		$search                     = array( "\n", "\r", "\t" );
		$replace                    = array( "", "", "" );
		$html                       = str_replace( $search, $replace, $html );
		$productVariationMaps       = array();
		$listAttributes             = array();
		$listAttributesDisplayNames = array();
		$propertyValueNames         = array();
		$listAttributesNames        = array();
		$listAttributesSlug         = array();
		$listAttributesIds          = array();
		$variationImages            = array();
		$regSku                     = '/window\.runParams\.productId="([\s\S]*?)";/im';
		preg_match( $regSku, $html, $match_product_sku );
		$variations = array();

		$ignore_ship_from = ( new self )->get_params( 'ignore_ship_from' );

		if ( count( $match_product_sku ) == 2 && $match_product_sku[1] ) {
			$attributes['sku'] = $match_product_sku[1];
			$reg               = '/var skuProducts=(\[[\s\S]*?]);/im';
			$regId             = '/<a[\s\S]*?data-sku-id="(\d*?)"[\s\S]*?>(.*?)<\/a>/im';
			$regTitle          = '/<dt class="p-item-title">(.*?)<\/dt>[\s\S]*?data-sku-prop-id="(.*?)"/im';
			$regGallery        = '/imageBigViewURL=(\[[\s\S]*?]);/im';
			$regCurrencyCode   = '/window\.runParams\.currencyCode="([\s\S]*?)";/im';
			$regDetailDesc     = '/window\.runParams\.detailDesc="([\s\S]*?)";/im';
			$regOffline        = '/window\.runParams\.offline=([\s\S]*?);/im';
			$regName           = '/class="product-name" itemprop="name">([\s\S]*?)<\/h1>/im';
			$regDescription    = '/<ul class="product-property-list util-clearfix">([\s\S]*?)<\/ul>/im';
			preg_match( $regOffline, $html, $offlineMatches );
			if ( count( $offlineMatches ) == 2 ) {
				$offline = $offlineMatches[1];
			}

			preg_match( $reg, $html, $matches );
			if ( $matches ) {
				$productVariationMaps = json_decode( $matches[1], true );
			}

			preg_match( $regDetailDesc, $html, $detailDescMatches );
			if ( $detailDescMatches ) {
				$attributes['description_url'] = $detailDescMatches[1];
			}

			preg_match( $regDescription, $html, $regDescriptionMatches );
			if ( $regDescriptionMatches ) {
				$attributes['short_description'] = $regDescriptionMatches[0];
			}

			$reg = '/<dl class="p-property-item">([\s\S]*?)<\/dl>/im';
			preg_match_all( $reg, $html, $matches );

			if ( count( $matches[0] ) ) {
				$match_variations = $matches[0];
				$title            = '';
				$titleSlug        = '';
				$reTitle1         = '/title="(.*?)"/mi';
				$reImage          = '/bigpic="(.*?)"/mi';
				$attr_parent_id   = '';
				for ( $i = 0; $i < count( $match_variations ); $i ++ ) {
					preg_match( $regTitle, $match_variations[ $i ], $matchTitle );

					if ( count( $matchTitle ) == 3 ) {
						$title          = $matchTitle[1];
						$title          = substr( $title, 0, strlen( $title ) - 1 );
						$titleSlug      = strtolower( trim( preg_replace( '/[^\w]+/i', '-', $title ) ) );
						$attr_parent_id = $matchTitle[2];
					}

					$attr   = array();
					$images = array();
					preg_match_all( $regId, $match_variations[ $i ], $matchId );

					if ( count( $matchId ) == 3 ) {
						foreach ( $matchId[1] as $matchID_k => $matchID_v ) {
							$listAttributesNames[ $matchID_v ] = $title;
							$listAttributesIds[ $matchID_v ]   = $attr_parent_id;
							$listAttributesSlug[ $matchID_v ]  = $titleSlug;
							preg_match( $reTitle1, $matchId[2][ $matchID_k ], $title1 );

							if ( count( $title1 ) == 2 ) {
								$attr[ $matchID_v ]           = $title1[1];
								$listAttributes[ $matchID_v ] = $title1[1];
							} else {
								$end                          = strlen( $matchId[2][ $matchID_k ] ) - 13;
								$attr[ $matchID_v ]           = substr( $matchId[2][ $matchID_k ], 6, $end );
								$listAttributes[ $matchID_v ] = $attr[ $matchID_v ];
							}

							preg_match( $reImage, $matchId[2][ $matchID_k ], $image );

							if ( count( $image ) == 2 ) {
								$images[ $matchID_v ]          = $image[1];
								$variationImages[ $matchID_v ] = $image[1];
							}
						}

					}
					$attributes['list_attributes']               = $listAttributes;
					$attributes['list_attributes_names']         = $listAttributesNames;
					$attributes['list_attributes_ids']           = $listAttributesIds;
					$attributes['list_attributes_slugs']         = $listAttributesSlug;
					$attributes['variation_images']              = $variationImages;
					$attributes['attributes'][ $attr_parent_id ] = $attr;
					if ( count( $images ) > 0 ) {
						$attributes['images'][ $attr_parent_id ] = $images;
					}
					$attributes['parent'][ $attr_parent_id ]             = $title;
					$attributes['attribute_position'][ $attr_parent_id ] = $i;
					$attributes['parent_slug'][ $attr_parent_id ]        = $titleSlug;
				}
			}

			preg_match( $regGallery, $html, $matchGallery );
			if ( count( $matchGallery ) == 2 ) {
				$attributes['gallery'] = json_decode( $matchGallery[1], true );
			}

			for ( $j = 0; $j < count( $productVariationMaps ); $j ++ ) {
				$temp = array(
					'skuId'         => strval( $productVariationMaps[ $j ]['skuId'] ),
					'skuPropIds'    => isset( $productVariationMaps[ $j ]['skuPropIds'] ) ? $productVariationMaps[ $j ]['skuPropIds'] : '',
					'skuAttr'       => isset( $productVariationMaps[ $j ]['skuAttr'] ) ? $productVariationMaps[ $j ]['skuAttr'] : '',
					'skuVal'        => $productVariationMaps[ $j ]['skuVal'],
					'image'         => '',
					'variation_ids' => array(),
				);

				if ( $temp['skuPropIds'] ) {
					$temAttr = array();
					$attrIds = explode( ',', $temp['skuPropIds'] );
					for ( $k = 0; $k < count( $attrIds ); $k ++ ) {
						$temAttr[ $attributes['list_attributes_slugs'][ $attrIds[ $k ] ] ] = $attributes['list_attributes'][ $attrIds[ $k ] ];
					}
					$temp['variation_ids'] = $temAttr;
					$temp['image']         = $attributes['variation_images'][ $attrIds[0] ];
				}
				array_push( $variations, $temp );
			}
			$attributes['variations'] = $variations;
			preg_match( $regName, $html, $matchName );
			if ( count( $matchName ) == 2 ) {
				$attributes['name'] = $matchName[1];
			}
			preg_match( $regCurrencyCode, $html, $matchCurrency );
			if ( count( $matchCurrency ) == 2 ) {
				$attributes['currency_code'] = $matchCurrency[1];
			}
		} else {
			$descriptionModuleReg = '/"descriptionModule":(.*?),"features":{},"feedbackModule"/';
			preg_match( $descriptionModuleReg, $html, $descriptionModule );
			if ( $descriptionModule ) {
				$descriptionModule             = json_decode( $descriptionModule[1], true );
				$attributes['sku']             = $descriptionModule['productId'];
				$attributes['description_url'] = $descriptionModule['descriptionUrl'];
			}

			$specsModuleReg = '/"specsModule":(.*?),"storeModule"/';
			preg_match( $specsModuleReg, $html, $specsModule );
			if ( $specsModule ) {
				$specsModule = json_decode( $specsModule[1], true );
				if ( isset( $specsModule['props'] ) ) {
					$attributes['specsModule'] = $specsModule['props'];
				}
			}
			$storeModuleReg = '/"storeModule":(.*?),"titleModule"/';
			preg_match( $storeModuleReg, $html, $storeModule );
			if ( $storeModule ) {
				$storeModule              = json_decode( $storeModule[1], true );
				$attributes['store_info'] = array(
					'name' => $storeModule['storeName'],
					'url'  => $storeModule['storeURL'],
					'num'  => $storeModule['storeNum'],
				);
			}
			$imagePathListReg = '/"imagePathList":(.*?),"name":"ImageModule"/';
			preg_match( $imagePathListReg, $html, $imagePathList );
			if ( $imagePathList ) {
				$imagePathList         = json_decode( $imagePathList[1], true );
				$attributes['gallery'] = $imagePathList;
			}
			$skuModuleReg = '/"skuModule":(.*?),"specsModule"/';
			preg_match( $skuModuleReg, $html, $skuModule );
			if ( count( $skuModule ) == 2 ) {
				preg_match_all( '/"skuId":(.*?),/', $skuModule[1], $sku_matches );
				$skuModule              = json_decode( $skuModule[1], true );
				$productSKUPropertyList = isset( $skuModule['productSKUPropertyList'] ) ? $skuModule['productSKUPropertyList'] : array();
				$china_id               = '';
				if ( is_array( $productSKUPropertyList ) && count( $productSKUPropertyList ) ) {
					for ( $i = 0; $i < count( $productSKUPropertyList ); $i ++ ) {
						$images            = array();
						$skuPropertyValues = $productSKUPropertyList[ $i ]['skuPropertyValues'];
						$attr_parent_id    = $productSKUPropertyList[ $i ]['skuPropertyId'];
						$skuPropertyName   = wc_sanitize_taxonomy_name( $productSKUPropertyList[ $i ]['skuPropertyName'] );
						if ( strtolower( $skuPropertyName ) == 'ships-from' && $ignore_ship_from ) {
							foreach ( $skuPropertyValues as $value ) {
								if ( $value['skuPropertySendGoodsCountryCode'] == 'CN' ) {
									$china_id = $value['propertyValueId'];
								}
							}
							continue;
						} //point 1
						$attr = array(
							'values'   => array(),
							'slug'     => $skuPropertyName,
							'position' => $i,
						);
						for ( $j = 0; $j < count( $skuPropertyValues ); $j ++ ) {
							$skuPropertyValue                               = $skuPropertyValues[ $j ];
							$propertyValueId                                = $skuPropertyValue['propertyValueId'];
							$propertyValueName                              = $skuPropertyValue['propertyValueName'];
							$propertyValueDisplayName                       = $skuPropertyValue['propertyValueDisplayName'];
							$listAttributesNames[ $propertyValueId ]        = $skuPropertyName;
							$listAttributesDisplayNames[ $propertyValueId ] = $propertyValueDisplayName;
							$propertyValueNames[ $propertyValueId ]         = $propertyValueName;
							$listAttributesIds[ $propertyValueId ]          = $attr_parent_id;
							$listAttributesSlug[ $propertyValueId ]         = $skuPropertyName;
							$attr['values'][ $propertyValueId ]             = $propertyValueDisplayName;
							$attr['values_sub'][ $propertyValueId ]         = $propertyValueName;
							$listAttributes[ $propertyValueId ]             = array(
								'name'     => $propertyValueDisplayName,
								'name_sub' => $propertyValueName,
								'color'    => isset( $skuPropertyValue['skuColorValue'] ) ? $skuPropertyValue['skuColorValue'] : '',
								'image'    => ''
							);
							if ( isset( $skuPropertyValue['skuPropertyImagePath'] ) && $skuPropertyValue['skuPropertyImagePath'] ) {
								$images[ $propertyValueId ]                  = $skuPropertyValue['skuPropertyImagePath'];
								$variationImages[ $propertyValueId ]         = $skuPropertyValue['skuPropertyImagePath'];
								$listAttributes[ $propertyValueId ]['image'] = $skuPropertyValue['skuPropertyImagePath'];
							}
						}

						$attributes['list_attributes']               = $listAttributes;
						$attributes['list_attributes_names']         = $listAttributesNames;
						$attributes['list_attributes_ids']           = $listAttributesIds;
						$attributes['list_attributes_slugs']         = $listAttributesSlug;
						$attributes['variation_images']              = $variationImages;
						$attributes['attributes'][ $attr_parent_id ] = $attr;
						$attributes['images'][ $attr_parent_id ]     = $images;

						$attributes['parent'][ $attr_parent_id ] = $skuPropertyName;
					}
				}

				$skuPriceList = $skuModule['skuPriceList'];
				if ( isset( $sku_matches[1] ) && is_array( $sku_matches[1] ) && count( $sku_matches[1] ) === ( $skuPriceList_count = count( $skuPriceList ) ) ) {
					for ( $j = 0; $j < count( $skuPriceList ); $j ++ ) {
						$temp = array(
							'skuId'              => strval( $sku_matches[1][ $j ] ),
							'skuAttr'            => isset( $skuPriceList[ $j ]['skuAttr'] ) ? $skuPriceList[ $j ]['skuAttr'] : '',
							'skuPropIds'         => isset( $skuPriceList[ $j ]['skuPropIds'] ) ? $skuPriceList[ $j ]['skuPropIds'] : '',
							'skuVal'             => $skuPriceList[ $j ]['skuVal'],
							'image'              => '',
							'variation_ids'      => array(),
							'variation_ids_sub'  => array(),
							'variation_ids_slug' => array(),
						);
						if ( $temp['skuPropIds'] ) {
							$temAttr    = array();
							$temAttrSub = array();
							$attrIds    = explode( ',', $temp['skuPropIds'] );

							if ( $china_id && ! in_array( $china_id, $attrIds ) && $ignore_ship_from ) {
								continue;
							}

							for ( $k = 0; $k < count( $attrIds ); $k ++ ) {
								if ( isset( $listAttributesDisplayNames[ $attrIds[ $k ] ] ) ) {
									$temAttr[ $attributes['list_attributes_slugs'][ $attrIds[ $k ] ] ]    = $listAttributesDisplayNames[ $attrIds[ $k ] ];
									$temAttrSub[ $attributes['list_attributes_slugs'][ $attrIds[ $k ] ] ] = $propertyValueNames[ $attrIds[ $k ] ];
									if ( ! empty( $attributes['variation_images'][ $attrIds[ $k ] ] ) ) {
										$temp['image'] = $attributes['variation_images'][ $attrIds[ $k ] ];
									}
								}
							}
							$temp['variation_ids']     = $temAttr;
							$temp['variation_ids_sub'] = $temAttrSub;
						}
						$variations [] = $temp;
					}
				} else {
					for ( $j = 0; $j < count( $skuPriceList ); $j ++ ) {
						$temp = array(
							'skuId'              => strval( $skuPriceList[ $j ]['skuId'] ),
							'skuAttr'            => isset( $skuPriceList[ $j ]['skuAttr'] ) ? $skuPriceList[ $j ]['skuAttr'] : '',
							'skuPropIds'         => isset( $skuPriceList[ $j ]['skuPropIds'] ) ? $skuPriceList[ $j ]['skuPropIds'] : '',
							'skuVal'             => $skuPriceList[ $j ]['skuVal'],
							'image'              => '',
							'variation_ids'      => array(),
							'variation_ids_sub'  => array(),
							'variation_ids_slug' => array(),
						);
						if ( $temp['skuPropIds'] ) {
							$temAttr    = array();
							$temAttrSub = array();
							$attrIds    = explode( ',', $temp['skuPropIds'] );

							if ( $china_id && ! in_array( $china_id, $attrIds ) && $ignore_ship_from ) {
								continue;
							}

							for ( $k = 0; $k < count( $attrIds ); $k ++ ) {
								if ( isset( $listAttributesDisplayNames[ $attrIds[ $k ] ] ) ) {
									$temAttr[ $attributes['list_attributes_slugs'][ $attrIds[ $k ] ] ]    = $listAttributesDisplayNames[ $attrIds[ $k ] ];
									$temAttrSub[ $attributes['list_attributes_slugs'][ $attrIds[ $k ] ] ] = $propertyValueNames[ $attrIds[ $k ] ];
									if ( ! empty( $attributes['variation_images'][ $attrIds[ $k ] ] ) ) {
										$temp['image'] = $attributes['variation_images'][ $attrIds[ $k ] ];
									}
								}
							}
							$temp['variation_ids']     = $temAttr;
							$temp['variation_ids_sub'] = $temAttrSub;
						}
						$variations [] = $temp;
					}
				}
				$attributes['variations'] = $variations;
			}
			$pageModuleReg = '/"titleModule":(.*?),"webEnv"/';
			preg_match( $pageModuleReg, $html, $pageModule );
			if ( count( $pageModule ) == 2 ) {
				$pageModule         = json_decode( $pageModule[1], true );
				$attributes['name'] = $pageModule['subject'];
			}

			$webEnvReg = '/"webEnv":(.*?)}}/';
			preg_match( $webEnvReg, $html, $webEnv );
			if ( count( $webEnv ) == 2 ) {
				$webEnv                      = json_decode( $webEnv[1] . '}', true );
				$attributes['currency_code'] = $webEnv['currency'];
			}
		}
		if ( $attributes['sku'] ) {
			$response['data'] = $attributes;
		} else {
			$response['status'] = 'error';
		}

		return $response;
	}

	public static function get_user_agent() {
		$user_agent_list = get_option( 'vi_wad_user_agent_list' );
		if ( ! $user_agent_list ) {
			$user_agent_list = '["Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.1.1 Safari\/605.1.15","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.80 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (X11; Ubuntu; Linux x86_64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10.14; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) HeadlessChrome\/60.0.3112.78 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; rv:60.0) Gecko\/20100101 Firefox\/60.0","Mozilla\/5.0 (Windows NT 6.1; Win64; x64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.90 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/64.0.3282.140 Safari\/537.36 Edge\/17.17134","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.131 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/64.0.3282.140 Safari\/537.36 Edge\/18.17763","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.80 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.1 Safari\/605.1.15","Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.1.1 Safari\/605.1.15","Mozilla\/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64; Trident\/7.0; rv:11.0) like Gecko","Mozilla\/5.0 (X11; Linux x86_64; rv:60.0) Gecko\/20100101 Firefox\/60.0","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36 OPR\/60.0.3255.151","Mozilla\/5.0 (Windows NT 6.1; WOW64; Trident\/7.0; rv:11.0) like Gecko","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.80 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.80 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/62.0.3202.94 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.157 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko\/20100101 Firefox\/66.0","Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko\/20100101 Firefox\/68.0","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/72.0.3626.109 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.90 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36 OPR\/60.0.3255.109","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36 OPR\/60.0.3255.170","Mozilla\/5.0 (Windows NT 6.3; Win64; x64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Windows NT 10.0; WOW64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (iPad; CPU OS 12_3_1 like Mac OS X) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.1.1 Mobile\/15E148 Safari\/604.1","Mozilla\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) HeadlessChrome\/60.0.3112.78 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 YaBrowser\/19.6.1.153 Yowser\/2.5 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/70.0.3538.77 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 YaBrowser\/19.4.3.370 Yowser\/2.5 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 YaBrowser\/19.6.0.1574 Yowser\/2.5 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Ubuntu Chromium\/74.0.3729.169 Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.131 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.0 Safari\/605.1.15","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.86 Safari\/537.36","Mozilla\/5.0 (Linux; U; Android 4.3; en-us; SM-N900T Build\/JSS15J) AppleWebKit\/534.30 (KHTML, like Gecko) Version\/4.0 Mobile Safari\/534.30","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.0.3 Safari\/605.1.15","Mozilla\/5.0 (Windows NT 6.1) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/11.1.2 Safari\/605.1.15","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.80 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/12.0.2 Safari\/605.1.15","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; WOW64; rv:45.0) Gecko\/20100101 Firefox\/45.0","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.90 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.157 Safari\/537.36","Mozilla\/5.0 (X11; Linux x86_64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.90 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.169 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/72.0.3626.121 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.86 Safari\/537.36","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/75.0.3770.100 Safari\/537.36","Mozilla\/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko\/20100101 Firefox\/60.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10.12; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit\/605.1.15 (KHTML, like Gecko) Version\/13.0 Safari\/605.1.15","Mozilla\/5.0 (Windows NT 6.1; rv:67.0) Gecko\/20100101 Firefox\/67.0","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36 OPR\/60.0.3255.151","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 Safari\/537.36 OPR\/60.0.3255.170","Mozilla\/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/74.0.3729.131 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/73.0.3683.103 YaBrowser\/19.4.3.370 Yowser\/2.5 Safari\/537.36","Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko\/20100101 Firefox\/56.0","Mozilla\/5.0 (Windows NT 6.1; WOW64; rv:56.0) Gecko\/20100101 Firefox\/56.0"]';
			update_option( 'vi_wad_user_agent_list', $user_agent_list );
		}
		$user_agent_list_array = json_decode( $user_agent_list, true );
		$return_agent          = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
		$last_used             = get_option( 'vi_wad_last_used_user_agent', 0 );
		if ( $last_used == count( $user_agent_list_array ) - 1 ) {
			$last_used = 0;
			shuffle( $user_agent_list_array );
			update_option( 'vi_wad_user_agent_list', json_encode( $user_agent_list_array ) );
		} else {
			$last_used ++;
		}
		update_option( 'vi_wad_last_used_user_agent', $last_used );
		if ( isset( $user_agent_list_array[ $last_used ] ) && $user_agent_list_array[ $last_used ] ) {
			$return_agent = $user_agent_list_array[ $last_used ];
		}

		return $return_agent;
	}

	public function get_params( $name = "" ) {

		if ( ! $name ) {
			return $this->params;
		} elseif ( isset( $this->params[ $name ] ) ) {
			return apply_filters( 'wooaliexpressdropship_params_' . $name, $this->params[ $name ] );
		} else {
			return false;
		}
	}

	public static function sku_exists( $sku = '' ) {
		$sku_exists = false;
		if ( $sku ) {
			$id_from_sku = wc_get_product_id_by_sku( $sku );
			$product     = $id_from_sku ? wc_get_product( $id_from_sku ) : false;
			$sku_exists  = $product && 'importing' !== $product->get_status();
		}

		return $sku_exists;
	}

	public static function set( $name, $set_name = false ) {
		if ( is_array( $name ) ) {
			return implode( ' ', array_map( array( 'VI_WOO_ALIDROPSHIP_DATA', 'set' ), $name ) );
		} else {
			if ( $set_name ) {
				return esc_attr__( str_replace( '-', '_', self::$prefix . $name ) );
			} else {
				return esc_attr__( self::$prefix . $name );
			}
		}
	}

	public static function get_aff_url( $sku, $u = false ) {

		$sku_array = $aff_urls = array();
		$u         = (bool) $u ? '&u=1' : '';

		if ( is_array( $sku ) ) {
			$sku_count = count( $sku );
			for ( $i = 0; $i <= $sku_count / 50; $i ++ ) {
				$sku_array[ $i ] = array_slice( $sku, $i * 50, 50 );
			}
		} else {
			$sku_array[] = array( $sku );
		}

		foreach ( $sku_array as $array ) {
			$list_id  = implode( ',', $array );
			$url      = "https://api.villatheme.com/wp-json/aliexpress/get_url/?aps={$list_id}{$u}";
			$res      = wp_remote_post( $url );
			$res_code = wp_remote_retrieve_response_code( $res );
			if ( $res_code == 200 ) {
				$body  = json_decode( wp_remote_retrieve_body( $res ) );
				$links = $body->links;

				if ( count( $links ) ) {
					foreach ( $links as $link ) {
						$id = ! empty( $link->url ) ? str_replace(
							array( 'https://www.aliexpress.com/item/', '.html' ), array( '', '' ), $link->url ) : '';
						if ( $id ) {
							$aff_urls[ $id ] = ! empty( $link->promotionUrl ) ? str_replace( 'http://s.click.aliexpress.com/', '', $link->promotionUrl ) : '';
						}
					}
				}
			}
		}

		return $aff_urls;
	}

	public function get_default( $name = "" ) {
		if ( ! $name ) {
			return $this->default;
		} elseif ( isset( $this->default[ $name ] ) ) {
			return apply_filters( 'wooaliexpressdropship_params_default_' . $name, $this->default[ $name ] );
		} else {
			return false;
		}
	}

	public function process_exchange_price( $price ) {
		if ( ! $price ) {
			return $price;
		}
		$rate = floatval( $this->get_params( 'import_currency_rate' ) );
		if ( $rate ) {
			$price = $price * $rate;
		}

		return $price;
	}

	public function process_price( $price, $is_sale_price = false ) {
		if ( ! $price ) {
			return $price;
		}
		$price_from      = $this->get_params( 'price_from' );
		$plus_value_type = $this->get_params( 'plus_value_type' );

		if ( $is_sale_price ) {
			$plus_sale_value = $this->get_params( 'plus_sale_value' );
			if ( is_array( $price_from ) && count( $price_from ) ) {
				$level_count = count( $price_from );
				$match       = $level_count - 1;
				for ( $i = 1; $i < $level_count; $i ++ ) {
					if ( $price < $price_from[ $i ] ) {
						$match = $i - 1;
						break;
					}
				}
				if ( $plus_sale_value[ $match ] < 0 ) {
					$price = 0;
				} else {
					$match_value = floatval( $plus_sale_value[ $match ] );
					switch ( $plus_value_type[ $match ] ) {
						case 'fixed':
							$price = $price + $match_value;
							break;
						case 'percent':
							$price = $price * ( 1 + $match_value / 100 );
							break;
						default:
							$price = $match_value;
					}
				}
			}
		} else {
			$plus_value = $this->get_params( 'plus_value' );
			if ( is_array( $price_from ) && count( $price_from ) ) {
				$level_count = count( $price_from );
				$match       = $level_count - 1;
				for ( $i = 1; $i < $level_count; $i ++ ) {
					if ( $price < $price_from[ $i ] ) {
						$match = $i - 1;
						break;
					}
				}
				$match_value = floatval( $plus_value[ $match ] );
				switch ( $plus_value_type[ $match ] ) {
					case 'fixed':
						$price = $price + $match_value;
						break;
					case 'percent':
						$price = $price * ( 1 + $match_value / 100 );
						break;
					default:
						$price = $match_value;
				}
			}
		}

		return $price;
	}

	public static function process_variation_sku( $sku, $variation_ids ) {
		$return = '';
		if ( is_array( $variation_ids ) && count( $variation_ids ) ) {
			foreach ( $variation_ids as $key => $value ) {
				$variation_ids[ $key ] = wc_sanitize_taxonomy_name( $value );
			}
			$return = $sku . '-' . implode( '-', $variation_ids );
		}

		return $return;
	}
}

new VI_WOO_ALIDROPSHIP_DATA();
