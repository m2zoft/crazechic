<?php

/**
 * Class VI_WOO_ALIDROPSHIP_Admin_System
 */
class VI_WOO_ALIDROPSHIP_Admin_Recommend {

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'menu_page' ), 30 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

	}

	public function admin_enqueue_scripts() {
		global $pagenow;
		$page = isset( $_REQUEST['page'] ) ? wp_unslash( sanitize_text_field( $_REQUEST['page'] ) ) : '';
		if ( $pagenow === 'admin.php' && $page === 'woo-ali-recommend' ) {
			wp_enqueue_style( 'vi-woo-alidropship-form', VI_WOO_ALIDROPSHIP_CSS . 'form.min.css' );
			wp_enqueue_style( 'vi-woo-alidropship-table', VI_WOO_ALIDROPSHIP_CSS . 'table.min.css' );
			wp_enqueue_style( 'vi-woo-alidropship-icon', VI_WOO_ALIDROPSHIP_CSS . 'icon.min.css' );
			wp_enqueue_style( 'vi-woo-alidropship-segment', VI_WOO_ALIDROPSHIP_CSS . 'segment.min.css' );
			wp_enqueue_style( 'vi-woo-alidropship-button', VI_WOO_ALIDROPSHIP_CSS . 'button.min.css' );
		}
	}

	public function page_callback() {
		$plugins = array(
			array(
				'slug' => 'woo-abandoned-cart-recovery',
				'name' => 'Abandoned Cart Recovery for WooCommerce',
				'desc' => __( 'Helps you to recovery unfinished order in your store. When a customer adds a product to cart but does not complete check out. After a scheduled time, the cart will be marked as “abandoned”. The plugin will start to send cart recovery email or facebook message to the customer, remind him/her to complete the order.', 'woo-alidropship' )
			),
			array(
				'slug' => 'woo-photo-reviews',
				'name' => 'Photo Reviews for WooCommerce',
				'desc' => __( 'An ultimate review plugin for WooCommerce which helps you send review reminder emails, allows customers to post reviews include product pictures and send thank you emails with WooCommerce coupons to customers.', 'woo-alidropship' )
			),
			array(
				'slug' => 'woo-orders-tracking',
				'name' => 'Order Tracking for WooCommerce',
				'desc' => __( 'Allows you to bulk add tracking code to WooCommerce orders. Then the plugin will send tracking email with tracking URLs to customers. The plugin also helps you to add tracking code and carriers name to your PayPal transactions. This option will save you tons of time and avoid mistake when adding tracking code to PayPal.', 'woo-alidropship' )
			),
//			array(
//				'slug' => 'woo-multi-currency',
//				'name' => 'Multi Currency for WooCommerce',
//				'desc' => __( 'Allows your customers to browse products and checkout in multiple currencies. The plugin converts WooCommerce product price, coupons, shipping price, taxes to customer-preferred currency. Customers can select the currency on front-end with widget, currencies bar. Or you can set the plugin to detect currency based on customer’s location, site’s language.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-lookbook',
//				'name' => 'Lookbook for WooCommerce',
//				'desc' => __( 'Helps your customer visualize your products, know how will it look in real life. This plugin is very useful for fashion shop. Before buying any clothes online, customers always wonder how will the product look like when they wear it. Lookbook for WooCommerce solves this problem by allowing you to create lookbook of your products with picture and nodes. Using nodes to marks your products on lookbook pictures. Customers can click on nodes to view product information and purchase.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-boost-sales',
//				'name' => 'Boost Sales for WooCommerce',
//				'desc' => __( 'WooCommerce extension that helps you increase the revenue of every single order by suggesting Up-selling and Cross-selling products to customers.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-notification',
//				'name' => 'Notification for WooCommerce',
//				'desc' => __( 'Helps you create social proof about a busy store. The plugin displays recent orders on a pop-up, with information about customer name, address, purchased items, product image, purchased time. You can set up to display recent WooCommerce orders. Or if your store is new and there are not many things to show, you can create virtual orders to display from a list of customer names, addresses and selected items. By displaying orders on front-end the plugin will create a social proof of a busy store with many customers and orders. Creating a sense of urgency for visitors, urge them to purchase products. Also by select products to display, you can introduce your new products.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-coupon-box',
//				'name' => 'Coupon Box for WooCommerce',
//				'desc' => __( 'Helps you collect emails from your visitor. Don’t waste your traffic, Email marketing is an old-but-gold marketing channel. Coupon Box for WooCommerce helps you turn traffic into email subscribers. The plugin displays a subscribe email pop-up to new visitors. Offering coupons and ask them to subscribe email. Then the plugin will generate and send coupon code the subscribed email address. You can check the subscribed email addresses in the plugin back-end or sync with your MailChimp account.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'import-shopify-to-woocommerce',
//				'name' => 'S2W – Import Shopify to WooCommerce',
//				'desc' => __( 'Helps you import all products from your Shopify store to WooCommerce. You just need to enter correct API and domain, change some options if you want then click import and wait. This plugin will sync data directly with your Shopify store via Shopify official API.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-free-shipping-bar',
//				'name' => 'Free Shipping Bar for WooCommerce',
//				'desc' => __( 'Uses free shipping as a marketing tool to increase order revenue. Free Shipping is a very useful marketing tool. Customers would spend more to get free shipping. Free Shipping Bar for WooCommerce will encourage customers to purchase more products to get free shipping. The plugin informs customers the free shipping minimum spend, how much they have ordered, and how much they will need to spend more to quantify. Congratulation message and link to Checkout page if they reached the minimum spend.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-thank-you-page-customizer',
//				'name' => 'Thank You Page Customizer for WooCommerce',
//				'desc' => __( 'The easiest way to customize a beautiful thank you page for your WooCommerce store. Simple, easy to use but still powerful and professional.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-product-builder',
//				'name' => 'Product Builder for WooCommerce – Custom PC Builder',
//				'desc' => __( 'Allows your customers to build a full product set from small parts step by step. The plugin works base on WooCommerce with many useful features like compatible, email completed product, attributes filters.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'sales-countdown-timer',
//				'name' => 'Sales Countdown Timer',
//				'desc' => __( 'Helps you maximize the revenue of your sale campaigns. The plugin displays sales countdown timer in single product pages and shop page. Encourage customers to purchase by creating urgency psychology.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-lucky-wheel',
//				'name' => 'Lucky Wheel for WooCommerce – Spin a Sale',
//				'desc' => __( 'Helps you convert your traffic into customers or at least, an email subscriber. It takes you a lot of work and money to have traffic to your store, so don’t waste it. Lucky Wheel for WooCommerce offers visitor entering their email address to spin the lucky wheel. Then the plugin will send the prize, a discount coupon, to the subscribed email address. With Lucky Wheel for WooCommerce, you will get email subscribers in the most comfortable way.', 'woo-alidropship' )
//			),
//			array(
//				'slug' => 'woo-advanced-product-information',
//				'name' => 'Advanced Product Information for WooCommerce',
//				'desc' => __( 'Display more intuitive information of products such as sale countdown, sale badges, who recently bought products, rank of products in their categories, available payment methods…', 'woo-alidropship' )
//			)
		);

		?>
        <style>
            .fist-col {
                min-width: 300px;
            }

            .vi-wad-plugin-name {
                font-weight: 600;
            }
        </style>
        <div class="">
            <h2><?php esc_html_e( 'Plugins recommend', 'woo-alidropship' ) ?></h2>
            <table cellspacing="0" id="status" class="vi-ui celled table">
                <thead>
                <tr>
                    <th><?php esc_html_e( 'Plugins', 'woo-alidropship' ); ?></th>
                    <th><?php esc_html_e( 'Description', 'woo-alidropship' ); ?></th>
                </tr>
                </thead>
                <tbody>
				<?php
				foreach ( $plugins as $plugin ) {
					$url          = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=' . $plugin['slug'] ), 'install-plugin_' . $plugin['slug'] );
					$details_link = self_admin_url( 'plugin-install.php?tab=plugin-information&amp;plugin=' . $plugin['slug'] );
					?>
                    <tr>
                        <td class="fist-col">
                            <div class="vi-wad-plugin-name">
								<?php echo $plugin['name'] ?>
                            </div>
                            <div>
                                <a href="<?php echo esc_url( $url ) ?>"
                                   target="_blank"><?php esc_html_e( 'Install', 'woo-alidropship' ); ?></a>
                            </div>
                        </td>
                        <td><?php echo $plugin['desc'] ?></td>
                    </tr>
					<?php
				}
				?>
                </tbody>
            </table>
        </div>
		<?php
	}

	/**
	 * Register a custom menu page.
	 */
	public function menu_page() {
		add_submenu_page(
			'woo-alidropship',
			esc_html__( 'Recommended plugins', 'woo-alidropship' ),
			esc_html__( 'Recommended plugins', 'woo-alidropship' ),
			'manage_options',
			'woo-ali-recommend',
			array( $this, 'page_callback' )
		);

	}
}
